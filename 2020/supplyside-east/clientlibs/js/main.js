(function($) {

    var engineUtils = {

        init: function() {
            this.ui.init();
        },
        
        ui: {

            init: function() {
                engineUtils.ui.layout();
                engineUtils.ui.plugins();           
                engineUtils.ui.screenResize();
            },

            layout: function() {

                /*     HEADER SECTION     */

                // Check if Header exist on DOM
                if($('.mainNav').length > 0){ 

                    //  Start - Sticky Header

                    $(window).scroll(function () {
                        var sticky = $('.custom-header'),
                        bannerH = $('.custom-header').innerHeight(),
                        scroll = $(window).scrollTop();

                        if (scroll >= 37) {   
                            sticky.addClass('fixed');                 
                            sticky.addClass('isfixed');
                            $('body').css('paddingTop',bannerH);
                        }
                        else {             
                            sticky.removeClass('fixed');   
                            $('body').css('paddingTop',0);                                                              
                            sticky.removeClass('isfixed');


                        }
                    });

                    //  End - Sticky Header

                    $('.custom-header').append($('.hamBurger'));

                }



            },

            plugins: function () {
                // Plugins Here!!!
            },

            screenResize: function () {
                $(window).resize(function () {
                    // Functions when screen resizes!!!
                });
            }

        }

    };

    $(document).ready(function() { engineUtils.init(); });
    
})(jQuery);