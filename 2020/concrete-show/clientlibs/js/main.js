(function() {
	"use strict";

    // global functions, fires when all resources and the document are ready
    function mConfig($bootstrap) {
    	// sticky navbar
    	new mascot.Sticky(".informa-header-menu", {
    		padding: ".headercontainer"
    	});
    }

    // check for document ready
    jQuery(document).ready(function() {
        if($('.mainNav').length > 0){
            $('.header--custom .header--custom-event-ctas .header--custom-event-ctas-column:eq(1) > div').prepend($('.hamBurger'));
        }

    	mReady.document = true;
    });

    // ready object
    var mReady = {};

    // interval checks
    mReady.all = setInterval(function() {

        // check for Bootstrap jQuery plugins,
        // store in case another jQuery library loads and overwrites plugins
        if (!mReady.bootstrap && $().collapse) {
			mReady.bootstrap = $;
        }

        // check for Mascot library
        if (!mReady.mascot && window.mascot) {
			mReady.mascot = true;
        }

        // all resources ready
        if (mReady.bootstrap && mReady.document && mReady.mascot) {
            clearInterval(mReady.all);

            // all resources ready, time for mConfig()
      		mConfig(mReady.bootstrap);
        }
    }, 10);

})();