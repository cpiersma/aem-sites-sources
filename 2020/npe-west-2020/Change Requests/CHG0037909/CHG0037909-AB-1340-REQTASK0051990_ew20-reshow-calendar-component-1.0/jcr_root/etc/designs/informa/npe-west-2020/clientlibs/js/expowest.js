(function() {
	"use strict";

	function mConfig($bootstrap) {

    // trigger event for custom components hook
    $(document).trigger("mascotReady", $bootstrap);

    // sticky navbar
    new mascot.Sticky(".all-fixed", {
      padding: ".menu-offset"
    });

    new mascot.ScrollTo(".initial-scroll-to", {
      offset: 75,
      duration: 1200
    });

    // footer year
    new mascot.FormatDate(".current-year");

  }

    $(window).scroll(function() {
        // if ($(this).scrollTop() > 100 ) {
        //     $('.custom-header').parents('.column-control').addClass('sticky-header');
        // } else {
        //     $('.custom-header').parents('.column-control').removeClass('sticky-header');
        // }
				//
		if ($(this).scrollTop() > 300 ) {
            $('.scrolltop:hidden').stop(true, true).fadeIn();
            $('.navbar-toggle').addClass('stuck');
        } else {
            $('.scrolltop').stop(true, true).fadeOut();
            $('.navbar-toggle').removeClass('stuck');
        }
    });

    // check for document ready
    jQuery(document).ready(function() {

			//Search feature
	      var search_but='<span id="search_Icon"><i class="fa fa-search" aria-hidden="true"></i></span>';
	      var email_but="<span class='email'><a href='/content/informa/npe-west/en/sign-up.html' target='_blank'><i class='fa fa-envelope' aria-hidden='true'></i></a></span>";

	      // $("#toggleDropdown_82_top_nav_copy_copy_co_edcfbc0e-6f8d-4950-84f1-c3c3e0d11d38_header_ipar_column_control_185581978_par-col-1_column_control_par-col-1_top_nav_copy_copy_co").after(search_but);

	      $(".header-cta .socialIcon").after(email_but);
	      $("#header_search,.header-search-element").append("<span class='trig-close'><i class='glyphicon glyphicon-remove'></i></span>");

	      $('#search_Icon').on('click', function() {
	           console.log("I am here");
	           $('#header_search,.header-search-element').addClass('active');
	           return false;
	      });

	      $('.trig-close').on('click', function() {
	           $('#header_search,.header-search-element').removeClass('active');
	           return false;
	      });

		// Check if top nav exist
        if($('.top-nav').length > 0){
            $('.navbar .mobileLayoutNav li').each(function() {
                $(this).find('.has-submenu').parent('li').addClass('menu-level-2'); // tag the 2nd level parent
            });
            $('.navbar .mobileLayoutNav li').each(function() {
                $(this).find('.level-3').parent('li').addClass('menu-level-3'); // tag the 3rd level parent
            });
        }

    	mReady.document = true;
    });

    // ready object
    var mReady = {};

    // interval checks
    mReady.all = setInterval(function() {
			// check for Bootstrap jQuery plugins,
	    // store in case another jQuery library loads and overwrites plugins
	    if (!mReady.bootstrap && $().collapse) {
	      mReady.bootstrap = $;
	    }

	    // check for Mascot library
	    if (!mReady.mascot && window.mascot) {
	      mReady.mascot = true;
	    }

			// all resources ready
	    if (mReady.bootstrap && mReady.document && mReady.mascot) {
	      clearInterval(mReady.all);

	      // all resources ready, time for mConfig()
	      mConfig(mReady.bootstrap);

	    }



    }, 10);

})();
