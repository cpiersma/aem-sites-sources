(function($) {
    
    var engineUtils = {
        
        init: function() {
            this.ui.init();
        },
        
        ui: {
            
            init: function() {
                engineUtils.ui.layout();
                engineUtils.ui.plugins();           
                engineUtils.ui.screenResize();
            },
            
            layout: function() {
                
                /*     HEADER SECTION     */
                
                // Check if Header exist on DOM
                if($('.mainNav').length > 0){ 
                    
                    //  Start - Sticky Header
                    


                    //  End - Sticky Header
                    
                    //  Start - Transfer Hamburger button to Header section
                    $('.custom-header').append($('.hamBurger'));
                    // End - Hamburger

                }
                
            },
            
            plugins: function () {
                // Plugins Here!!!
            },

            screenResize: function () {
                $(window).resize(function () {
                    // Functions when screen resizes!!!
                });
            }
            
        }
        
    };

    $(document).ready(function() { engineUtils.init(); });

})(jQuery);