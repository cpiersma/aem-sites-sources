$( window ).load(function() {

  var object = [
      {
        group: "Premier Sponsors",
        exhibitors: [
            {
              name: "LG Signature",
              url: "http://www.thesignaturekitchen.com/"
            },
            {
              name: "Toyota",
              url: "http://www.toyota.com/prius"
            },
            {
              name: "Toyota Prius",
              url: "http://www.toyota.com/prius"
            }
        ]
      },{
        group: "Sponsors",
        exhibitors: [
            {
              name: "AGS Stainless",
              url: "https://agsstainless.com/"
            },
            {
              name: "Art + Intention",
              url: "http://www.anneward.com"
            },
            {
              name: "Astek Wall Coverings",
              url: "http://www.astekwallcovering.com/"
            },
            {
              name: "Atgstores.com",
              url: "https://www.atgstores.com/"
            },
            {
              name: "Belmont Cabinets",
              url: "https://bellmontcabinets.com/"
            },
            {
              name: "Benjamin Moore",
              url: "http://www.benjaminmoore.com/en-us/welcome-to-benjamin-moore"
            },
            {
              name: "Cocoon9",
              url: "http://cocoon9.com/"
            },
            {
              name: "Curio Design",
              url: "http://curiodesign.com/index.php"
            },
            {
              name: "Decorist",
              url: "https://www.decorist.com/dwell/"
            },
            {
              name: "Dunn Edwards",
              url: "https://www.dunnedwards.com/"
            },
            {
              name: "ExpoMarkit",
              url: "http://expomarkit.com/"
            },
            {
              name: "Fireclay Tile",
              url: "https://www.fireclaytile.com/"
            },
            {
              name: "Haier",
              url: "http://www.haieramerica.com/"
            },
            {
              name: "Integrity",
              url: "http://www.marvin.com/integrity"
            },
            {
              name: "James Hardie",
              url: "https://www.jameshardie.com/"
            },
            {
              name: "La Cantina Doors",
              url: "http://www.lacantinadoors.com/ "
            },
            {
              name: "Living Homes",
              url: "http://www.livinghomes.net/primer.html"
            },
            {
              name: "Modern House Numbers",
              url: "http://www.modernhousenumbers.com/"
            },
            {
              name: "Monogram",
              url: "http://dwell.com/monogram"
            },
            {
              name: "Nana Wall",
              url: "http://www.nanawall.com"
            },
            {
              name: "Sage",
              url: "http://www.sagebyhughes.com/"
            },
            {
              name: "SoCal Gas",
              url: "https://www.socalgas.com/"
            },
            {
              name: "Suite Plants",
              url: "http://www.suiteplants.com/"
            },
            {
              name: "TOTO",
              url: "http://www.totousa.com/"
            },
            {
              name: "Urban Floor",
              url: "http://www.urbanfloor.com/"
            },
            {
              name: "VinoTemp",
              url: "http://www.vinotemp.com/"
            },
            {
              name: "Vintage View Wine Storage Systems",
              url: "http://vintageview.com/"
            }
        ]
      },
      {
        group: "Partners",
        exhibitors: [
            {
              name: "AIA Los Angeles",
              url: "http://www.aialosangeles.org/"
            },
            {
              name: "American Society of Landscape Architects",
              url: "https://www.asla.org/default.aspx"
            },
            {
              name: "APLD (Association of Professional Landscape Designers)",
              url: "http://www.apld.com/"
            },
            {
              name: "Arcbazar",
              url: "http://www.arcbazar.com/"
            },
            {
              name: "Archello",
              url: "http://us.archello.com/en"
            },
            {
              name: "Archilovers",
              url: "http://www.archilovers.com/"
            },
            {
              name: "Archinect",
              url: "http://archinect.com"
            },
            {
              name: "Archiproducts",
              url: "http://www.archiproducts.com/"
            },
            {
              name: "The Architects Newspaper",
              url: "http://www.archpaper.com/"
            },
            {
              name: "ASID",
              url: "https://www.asid.org/"
            },
            {
              name: "AWA+D",
              url: "http://awaplusd.org/"
            },
            {
              name: "AZURE",
              url: "http://www.azuremagazine.com/"
            },
            {
              name: "Bustler",
              url: "http://bustler.net/"
            },
            {
              name: "California Homes",
              url: "http://www.calhomesmagazine.com/"
            },
            {
              name: "California Home + Design",
              url: "http://www.californiahomedesign.com/"
            },
            {
              name: "Cover",
              url: "http://cover-magazine.com/"
            },
            {
              name: "Design\milk",
              url: "http://design-milk.com/"
            },
            {
              name: "Design Space",
              url: "http://designspacemagazine.com/"
            },
            {
              name: "Digs",
              url: "http://digs.net/"
            },
            {
              name: "dwell HOMES",
              url: "http://www.dwellhomes.com"
            },
            {
              name: "Fabrik",
              url: "http://fabrik.la/"
            },
            {
              name: "FORM",
              url: "http://www.formmag.net/"
            },
            {
              name: "Gary Sinise",
              url: "https://www.garysinisefoundation.org/"
            },
            {
              name: "Gray Magazine",
              url: "http://www.graymag.com/"
            },
            {
              name: "IDS",
              url: "http://www.interiordesignshow.com/"
            },
            {
              name: "IFDM",
              url: "http://www.ifdm.it/it/"
            },
            {
              name: "IIDA",
              url: "http://iida-socal.org/"
            },
            {
              name: "IIDEX Canada",
              url: "http://www.iidexcanada.com/"
            },
            {
              name: "Interiors",
              url: "http://interiorsmagazine.com/"
            },
            {
              name: "Interior Design Society",
              url: "http://www.interiordesignsociety.org/"
            },
            {
              name: "Interior Directory",
              url: "http://www.theinteriordirectory.com/"
            },
            {
              name: "Jamie Durie",
              url: "https://www.jamiedurie.com/"
            },
            {
              name: "J Evans",
              url: "http://www.jevanscustomsolutions.com/"
            },
            {
              name: "LA Art Party",
              url: "http://laartparty.com/"
            },
            {
              name: "LADF",
              url: "http://www.ladesignfestival.org/"
            },
            {
              name: "LA Film Festival",
              url: "http://www.filmindependent.org/la-film-festival/"
            },
            {
              name: "Laist",
              url: "http://laist.com/"
            },
            {
              name: "Luxury Pools",
              url: "http://www.luxurypools.com/"
            },
            {
              name: "Modern Magazine",
              url: "http://www.modernmag.com/"
            },
            {
              name: "MSI",
              url: "https://www.msistone.com/"
            },
            {
              name: "NKBA",
              url: "http://www.nkba.org/"
            },
            {
              name: "NTCA",
              url: "http://www.tile-assn.com/"
            },
            {
              name: "Ocean Home",
              url: "http://www.oceanhomemag.com/"
            },
            {
              name: "Paintbox Press",
              url: "http://paintboxpress.com/"
            },
            {
              name: "Pasadena Museum of California Art",
              url: "http://pmcaonline.org/"
            },
            {
              name: "Roomhints",
              url: "https://www.roomhints.com/"
            },
            {
              name: "RST Brands",
              url: "http://www.rstbrands.com/"
            },
            {
              name: "Season",
              url: "http://www.seasonslandscaping.com/home"
            },
            {
              name: "SMUD",
              url: "https://www.smud.org/en/index.htm"
            },
            {
              name: "Southwest greens",
              url: "http://www.socalswg.com"
            },
            {
              name: "Sustainable furnishings council",
              url: "http://www.sustainablefurnishings.org/"
            },
            {
              name: "The Retail Observer",
              url: "http://www.retailobserver.com"
            },
            {
              name: "USGBC (L.A.)",
              url: "http://usgbc-la.org/"
            },
            {
              name: "V2com",
              url: "http://v2com-newswire.com/en"
            }
        ]
      }
  ]


  var sliding;
  var holder;
  var width;

  animateSlide = function (groupClass) {
    var holder = $("#" + groupClass),
        list = holder.find("ul"),
        length = list.length;

    if (length === 1) {
      list.first().show();
    } else {
      var first = list.first(),
          last = list.last(),
          position = first;

      $(first).show();

      setInterval(function () {

        var x = $(position[0]).attr("class"),
            y = $(last[0]).attr("class");

        if (x === y) {
          original = last;
          position = first;
          createFade(original, position);
        } else {
          original = position;
          position = position.next('ul');
          createFade(original, position);
        }

        function createFade(o, p) {
          $(original).fadeOut(1000);
          $(position).delay(1000).fadeIn(1000);
        }

      }, 5000);

    }

  }

  drawArray = function (holder, header, list) {

    var groupClass = header.replace(/\s/g, '') + "-footer";

    $("." + holder).append("<div class='slider' id='" + groupClass + "'><h2><span>" + header + "</span></h2></div>");

    for (var n in list) {
      var array = list[n];
      $("#" + groupClass).append("<ul class='" + groupClass + "-" + ++n + "' rel='" + n + "' style='display: none;'></ul>");


      for (var i in array) {


        var exhibitor = array[i],
            name = exhibitor.name,
            url = exhibitor.url,
            exhClass = name.replace(/[^\w\s]/gi, '').toLowerCase().replace(/\s/g, '');

        $("ul." + groupClass + "-" + n).append("<li><a href='" + url + "' target='_blank' class='" + exhClass + "' style='background: url(/content/dam/dam_images/sponsors/" + exhClass + ".png) no-repeat 0 0 transparent'>" + name + "</a></li>");

      }
    }

    animateSlide(groupClass)

  }

  setList = function (holder, width) {
    holder = holder;
    width = width;
    var limiter = Math.floor($("." + holder).width() / width);

    $.each(object, function (i, n) {
      exhArr = [];

      var exh = n.exhibitors,
        header = n.group,
        temp = exh.slice();

      do {
        exhArr.push(temp.splice(0, limiter));
      }
      while (temp.length > 0)

      drawArray(holder, header, exhArr);
    });
  }

  if (location.pathname !== "/account/logon") {
    setList("footer-logo", 160);

    $(window).resize(function () {
      $(".footer-logo").html('');
      setList("footer-logo", 160);
    });
  }
});