(function() {
	"use strict";

    $(window).scroll(function() {
        if ($(this).scrollTop() > 0 ) {
            $('.custom-header').parents('.column-control').addClass('sticky-header');
        } else {
            $('.custom-header').parents('.column-control').removeClass('sticky-header');
        }

		if ($(this).scrollTop() > 300 ) {
            $('.scrolltop:hidden').stop(true, true).fadeIn();
            $('.navbar-toggle').addClass('stuck');
        } else {
            $('.scrolltop').stop(true, true).fadeOut();
            $('.navbar-toggle').removeClass('stuck');
        }
    });

    // check for document ready
    jQuery(document).ready(function() {

		// Check if top nav exist
        if($('.top-nav').length > 0){
            $('.navbar .mobileLayoutNav li').each(function() {
                $(this).find('.has-submenu').parent('li').addClass('menu-level-2'); // tag the 2nd level parent
            });
            $('.navbar .mobileLayoutNav li').each(function() {
                $(this).find('.level-3').parent('li').addClass('menu-level-3'); // tag the 3rd level parent
            });
        }

    	mReady.document = true;
    });

    // ready object
    var mReady = {};

    // interval checks
    mReady.all = setInterval(function() {

        // all resources ready
        if (mReady.document) {
            clearInterval(mReady.all);
        }
    }, 10);

})();
