(function() {
  "use strict";

  // global functions, fires when all resources and the document are ready
  function mConfig($bootstrap) {

    // trigger event for custom components hook
    $(document).trigger("mascotReady", $bootstrap);


    // sticky navbar
    new mascot.Sticky(".informa-header-menu", {
      padding: ".headercontainer"
    });

    new mascot.ScrollTo(".initial-scroll-to", {
      offset: 60,
      duration: 1200
    });

    // footer year
    new mascot.FormatDate(".current-year");

  }

  // check for document ready
  jQuery(document).ready(function() {
    mReady.document = true;
  });

  // ready object
  var mReady = {};

  // interval checks
  mReady.all = setInterval(function() {

    // check for Bootstrap jQuery plugins,
    // store in case another jQuery library loads and overwrites plugins
    if (!mReady.bootstrap && $().collapse) {
      mReady.bootstrap = $;
    }

    // check for Mascot library
    if (!mReady.mascot && window.mascot) {
      mReady.mascot = true;
    }

    // all resources ready
    if (mReady.bootstrap && mReady.document && mReady.mascot) {
      clearInterval(mReady.all);

      // all resources ready, time for mConfig()
      mConfig(mReady.bootstrap);

    }
  }, 10);

})();
