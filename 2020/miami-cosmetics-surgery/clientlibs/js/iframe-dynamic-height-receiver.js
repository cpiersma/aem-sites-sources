var aemIframeResize = function (event) { 
    "use strict";
    if (isNaN(event.data)) { 
        return;
    }
    var directoryIframe = document.getElementsByClassName("euromedicom-iframe");
    if (directoryIframe) {
        directoryIframe[0].style.height = event.data + "px";
    }
};
    
function resetIframe(event) {
    "use strict";
    if (event.data !== "resetIframe") {
        return;
    }
    var directoryIframe = document.getElementsByClassName("euromedicom-iframe");
    if (directoryIframe) {
        directoryIframe[0].style.height = null;
    }

    if (directoryIframe[0].contentWindow) {
        directoryIframe[0].contentWindow.postMessage("resetComplete", "*");
    }
}

window.addEventListener("message", aemIframeResize, false);
window.addEventListener("message", resetIframe, false);
