(function() {
	"use strict";

	// global functions, fires when all resources and the document are ready
	function mConfig($bootstrap) {

		// // trigger event for custom components hook
		// $(document).trigger("mascotReady", $bootstrap);

		// // sticky navbar
		// new mascot.Sticky(".header__columns", {
		// 	padding: ".headercontainer"
		// });

	}

	// ready object
	var mReady = {};

	// check for document ready
	jQuery(document).ready(function() {
		// add toggle menu
		if($('.mainNav').length > 0){ // Check if Header exist on DOM
            $('.header__columns-container-column:eq(1) > div').prepend($('.hamBurger'));
		}
		// end: toggle menu

        // add search icon to the main nav
		$('.mainNav .container .navbar-element .navbar-nav').append("<li class='search-btn hidden-xs'><a class='trig-search'><small class='fas fa-search'></small></a></li>");

        $('.trig-search').on('click', function(){
			$('.header__strip .header-search-element').addClass('active');
			return false;
		});

		// add close icon to the search form popup
		$(".header__strip .header-search-element").append("<div class='trig-close'><span class='fas fa-times'></span></div>");

        $('.trig-close').on('click', function(){
			$('.header__strip .header-search-element').removeClass('active');
			return false;
		});

        // close when clicked outside the search box
        $('body').on('click', function(e){
            var searchBlock = $('.header__strip .header-search-element');
            //alert(searchBlock.is(e.target));

            if(!searchBlock.is(e.target) && searchBlock.has(e.target).length === 0){
                searchBlock.removeClass('active');
            }
        });
		// end: search icon

        // add scroll to top functionality
        $('body').append("<div class='scrolltop'><div class='scroll icon'></div></div>");
        $('.scrolltop').hide();
        $(".scroll").click(function(){
            $("html,body").animate({
                scrollTop: 0
            },"1000");
            return false;
        });

        $(window).scroll(function() {
            if ($(this).scrollTop() > 300 ) {
                $('.scrolltop:hidden').stop(true, true).fadeIn();
            } else {
                $('.scrolltop').stop(true, true).fadeOut();
            }
        });

		mReady.document = true;
	});

	// interval checks
	mReady.all = setInterval(function() {
		// check for Bootstrap jQuery plugins,
		// store in case another jQuery library loads and overwrites plugins
		if (!mReady.bootstrap && $().collapse) {
			mReady.bootstrap = $;
		}
		// check for Mascot library
		if (!mReady.mascot && window.mascot) {
			mReady.mascot = true;
		}
		// all resources ready
		if (mReady.bootstrap && mReady.document && mReady.mascot) {
			clearInterval(mReady.all);
			// All resources ready, time for mConfig()
			mConfig(mReady.bootstrap);
		}
	}, 10);
})();

