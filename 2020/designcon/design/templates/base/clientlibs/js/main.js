(function() {
	"use strict";

	$(document).ready(function() {
		//////////////////////////////
		// boilerplate
		//////////////////////////////

		var $hamburger = $('.hamBurger');
		var $mobileTopNav = $('.top-nav .mainNav .navbar .container .navbar-element');

		var requestAnimationFrameID = null;

		function startLoop() {
			stopLoop();
			runLoop();

			setTimeout(stopLoop, 1000);
		}

		function runLoop() {
			var hamburgerPositionTop = $('.boilerplate').height() || 0;
			$hamburger.css('top', hamburgerPositionTop);
			$mobileTopNav.css('top', hamburgerPositionTop);

			requestAnimationFrameID = requestAnimationFrame(runLoop);
		}

		function stopLoop() {
			if (requestAnimationFrameID) {
				cancelAnimationFrame(requestAnimationFrameID);
				requestAnimationFrameID = null;
			}
		}

		$('.boilerplate .informa .row div.dropdown button').click(startLoop);

		$(window).scroll(function() {
			if ($(this).scrollTop() > informaBarHeight) {
				stopLoop();

				$hamburger.removeAttr('style');
				$mobileTopNav.removeAttr('style');
			}
			else {
				startLoop();
			}
		});


		//////////////////////////////
		// custom header
		//////////////////////////////

		var customHeaderHeight = 0;
		var informaBarHeight = 0;
		function updateCustomHeaderValues() {
			customHeaderHeight = $('.custom-header').height();
			informaBarHeight = $('.informa-bar').height() || 0;
		}

		var $html = $('html');
		var $container = $('#body_container_wrapper');
		if (!$container || $container.length === 0) {
			$container = $('body');
			console.warn('Sticky header: body is used because the content is container missing');
		}

		$(window).scroll(function() {
			if ($(this).scrollTop() > informaBarHeight) {
				$html.addClass('sticky-header');

				var marginTopNeeded = !!($('.custom-header').css('position') === 'fixed');
				if (marginTopNeeded) {
					$container.css('margin-top', customHeaderHeight);
				}
			} else {
				$html.removeClass('sticky-header');
				$container.css('margin-top', 0);
			}
		});

		updateCustomHeaderValues();
		$(window).resize(updateCustomHeaderValues);


		//////////////////////////////
		// top nav
		//////////////////////////////

		// mobile fix for opening dropdown-menu
		$('.top-nav .mainNav .navbar .container .navbar-element .navbar-nav .mainMenuTopnav a.has-submenu').click(function(e) {
			var target = e.target || {};
			var tagName = (target.nodeName || target.tagName || '').toLowerCase();
			if (tagName !== 'small') {
				e.preventDefault();
				e.stopPropagation();
			}
		});
	});
})();
