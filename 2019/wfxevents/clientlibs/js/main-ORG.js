(function() {

  // Fires when all resources and the document are ready
  function mConfig($bootstrap) {

    // trigger event for custom components hook
    $(document).trigger("mascotReady", $bootstrap);

    // Documentation here: http://pentonweb.com/docs/mascot/javascript/

    // data URLs
    var exhibitorData = "https://pentonweb.com/api/a2z/2016/ExhibitorProvider.php?id=786";
    var conferenceData = "https://pentonweb.com/api/a2z/2016/ConferenceProvider.php?id=786";

    // a2z exhibitor list via Handlebars template
    new mascot.Exhibitor(
      // view
      "#exhibitor-list", {
        // a2z data URL
        url: exhibitorData,
        // controller UI
        controllerView: "#exhibitor-list-controller",
        // base a2z link
        a2zLink: "http://conference.wfxevents.com/wfx18/Public/"
      }
    );

    // a2z session list via Handlebars template
    new mascot.Session(
      // view
      "#session-list", {
        // a2z data URL
        url: conferenceData,
        // controller UI
        controllerView: "#session-list-controller",
        // Link to speaker page in AEM
        speakerLink: "speakers.html"
      }
    );

    new mascot.Speaker(
      // view
      "#speaker-list", {
        // a2z data URL
        url: conferenceData,
        // controller UI
        controllerView: "#speaker-list-controller",
        // Link to sessions page in AEM
        sessionLink: "sessions.html",
        defaultSpeakerImage: "https://pentonweb.com/events/2018/wfx/core/img/favicons/wfx-brand-main.png"
      }
    );
    new mascot.Opps("#opps-list", {
      url: "https://opps.pentonweb.com/category/wfx-event-2018/?json=get_posts&post_type=opportunity&orderby=title&order=asc&count=-1&exclude=comments,comment_count,comment_status,author,date,modified,tags,url,status", // API URL
      controllerView: "#opps-list-controller"
    });
    new mascot.Sponsor("#sponsor-list", {
        url: "https://sponsortool.penton.com/json/1568731",
        levelOrder: "1,2,3,4,5",
        heading: {
          "1": "Platinum",
          "2": "Gold",
          "3": "Silver",
          "4": "Bronze",
          "5": "Nickel"
        },
        logoClass: {
          "2": "sponsor-logo--2",
          "3": "sponsor-logo--3"
        },
        logoType: {
          "1": "sponsorLogoDescription"
        },
        columnClass: {
          "2": "col-md-6 col-lg-4",
          "3": "col-xs-6 col-lg-3"
        }
      });


  }

  // Ready object
  var mReady = {};

  // Check for document ready
  jQuery(document).ready(function() {
    mReady.document = true;
  });

  // Interval checks
  mReady.all = setInterval(function() {
    // Check for Bootstrap jQuery plugins,
    // store in case another jQuery library loads and overwrites plugins
    if (!mReady.bootstrap && $().collapse) {
      mReady.bootstrap = $;
    }
    // Check for Mascot library
    if (!mReady.mascot && window.mascot) {
      mReady.mascot = true;
    }
    // All resources ready
    if (mReady.bootstrap && mReady.document && mReady.mascot) {
      clearInterval(mReady.all);
      // All resources ready, time for mConfig()
      mConfig(mReady.bootstrap);
    }
  }, 10);

})();
