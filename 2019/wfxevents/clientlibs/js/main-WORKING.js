(function() {

  // Fires when all resources and the document are ready
  function mConfig($bootstrap) {

    // trigger event for custom components hook
    $(document).trigger("mascotReady", $bootstrap);

    // sticky navbar
    new mascot.Sticky(".informa-header-menu", {
      padding: ".headercontainer"
    });

    new mascot.ScrollTo(".initial-scroll-to", {
      offset: 60,
      duration: 1200
    });

    // footer year
    new mascot.FormatDate(".current-year");

  }

  // Ready object
  var mReady = {};

  // Check for document ready
  jQuery(document).ready(function() {
    mReady.document = true;
  });

  // Interval checks
  mReady.all = setInterval(function() {
    // Check for Bootstrap jQuery plugins,
    // store in case another jQuery library loads and overwrites plugins
    if (!mReady.bootstrap && $().collapse) {
      mReady.bootstrap = $;
    }
    // Check for Mascot library
    if (!mReady.mascot && window.mascot) {
      mReady.mascot = true;
    }
    // All resources ready
    if (mReady.bootstrap && mReady.document && mReady.mascot) {
      clearInterval(mReady.all);
      // All resources ready, time for mConfig()
      mConfig(mReady.bootstrap);
    }
  }, 10);

})();
